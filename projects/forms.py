from django.forms import ModelForm
from projects.models import Project, Gallery


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = (
            "name",
            "description",
            "owner",
        )

class GalleryForm(ModelForm):
    class Meta:
        model = Gallery
        fields = (
            "title",
            "description",
            "img",
        )
