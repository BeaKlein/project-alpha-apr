from django.urls import path
from projects.views import list_project, show_project, create_project, upload_image, show_gallery

urlpatterns = [
    path("", list_project, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
    path("image/", upload_image, name="upload_images"),
    path("gallery/<int:id>/", show_gallery, name="show_gallery"),
]
