from django.db import models
from django.contrib.auth.models import User


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        User, related_name=("projects"), on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return self.name

class Gallery(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField()
    img = models.URLField()
    project = models.ForeignKey(
        Project,
        related_name=("gallerys"),
        on_delete=models.CASCADE
    )
