from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project, Gallery
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm, GalleryForm


@login_required()
def list_project(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {"list_projects": list_projects}
    return render(request, "projects/list.html", context)


@login_required()
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {"show_project": project}
    return render(request, "projects/details.html", context)


@login_required()
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)

@login_required()
def upload_image(request):
    if request.method == "POST":
        form = GalleryForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("show_projects")
    else:
        form = GalleryForm()
    context = {
        "form": form
    }
    return render(request, "projects/upload_image.html", context)

@login_required()
def show_gallery(request, id):
    my_gallery = get_object_or_404(Gallery, id=id)
    context = {
        "show_gallery": my_gallery
    }
    return render(request, "projects/gallery.html", context)
