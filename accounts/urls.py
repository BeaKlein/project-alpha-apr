from django.urls import path
from accounts.views import show_login, user_logout, user_signup

urlpatterns = [
    path("login/", show_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", user_signup, name="signup"),
]
